
import os
import time
import json
import pickle
import pandas as pd
import numpy as np

from pathlib import Path
from datetime import datetime as dt
from dotmap import DotMap
from riotwatcher import LolWatcher, ApiError
from requests import HTTPError



def serialize(obj,file_path,module=None):
	if module is None:
		module=pickle
	with open(str(file_path),mode="wb") as file:
		module.dump(obj,file,protocol=-1)

def deserialize(file_path,module=None):
	if module is None:
		module=pickle
	obj=None
	try:
		with open(str(file_path),mode="rb") as file:
			obj=module.load(file)
	except FileNotFoundError:
		print(f"{str(file_path)} not found.")
		obj=None
	return obj




class LolDataFetcher:

	# PREFIXES=["","k","M","T"]

	MIN_WAIT_TIME=1.21

	# MATCHES_MAX_INDEX=998
	EXTENSION_DATABASE="lol-data"
	EXTENSION_INFOLIST="lol-info"

	INFOLIST_DTYPE_FROM_HEADER={
		"match_id":"string",
		"created":"datetime64[ns]",
		"won":"bool",
		"champion":"string",
		"kills":"int",
		"deaths":"int",
		"assists":"int",
		"byte_start":"int",
		"byte_count":"int",
	}

	def __init__(self,account_info_path):
		with open(account_info_path,"r") as file:
			self.account_info=DotMap(json.load(file))
		# print(self.account_info.puuid)
		self.watcher=LolWatcher(self.account_info.api_key)
		self.last_api_call=dt.now()

	def _delay_api_call(self):
		delta=dt.now()-self.last_api_call
		delta=delta.seconds
		if delta<LolDataFetcher.MIN_WAIT_TIME:
			time.sleep(LolDataFetcher.MIN_WAIT_TIME-delta)
		self.last_api_call=dt.now()

	# def _load_df(self,season):

	def _api_call(self,fun,args=[],kwargs={}):
		self._delay_api_call()
		not_successfull=True
		while not_successfull:
			try:
				result=fun(*args,**kwargs)
				not_successfull=False
			except HTTPError as err:
				errno=str(err)[:3]
				if errno=="503":
					print("\nServer error 503. Waiting 5s for it to resolve and then retrying.")
					time.sleep(5)
				elif errno=="404":
					print("\nServer error 404. Skipping match.")
					result=None
					not_successfull=False
				else:
					raise err
		return result


	def _get_match_by_index(self,index,count=1,newline=True):
		print(f"Retrieving index {index}.",end="")
		match_ids=self._api_call(
			self.watcher.match.matchlist_by_puuid,
			[self.account_info.region,self.account_info.puuid],
			{"start":index,"count":1})

		if match_ids is None:
			return None

		if len(match_ids)<1:
			print(" No match found.")
			return None
		else:
			match_id=match_ids[0]
		print(" Got match id.",end="")
		match=self._api_call(
			self.watcher.match.by_id,
			[self.account_info.region,match_id])
		if match is None:
			got=""
		else:
			got=" Got match."
		print(got,end="\n" if newline else "")
		return match

	def _get_player(self,match):
		players=match["info"]["participants"]
		for player in players:
			if player["puuid"]==self.account_info.puuid:
				return player

	def _won_this_match(self,match):
		teams=match["info"]["teams"]
		players=match["info"]["participants"]
		winning_team_id=None
		for team in teams:
			if team["win"]:
				winning_team_id=team["teamId"]
		player_team_id=self._get_player(match)["teamId"]

		if winning_team_id is None:
			print("No team won?!")
			return False

		return player_team_id==winning_team_id

	def _file_name_from_season(self,season):
		return f"season{season:02}"

	def _new_season_info_list(self):
		df=pd.DataFrame(
			{head:pd.Series([],dtype=typ)
			for head,typ in LolDataFetcher.INFOLIST_DTYPE_FROM_HEADER.items()})
		return df

	def _append_to_df(self,df,content):
		return pd.concat([df,self._new_season_info_row(content)],ignore_index=True)

	def _new_season_info_row(self,content):
		df=pd.DataFrame(
			{head:pd.Series([content[head]],dtype=typ)
			for head,typ in LolDataFetcher.INFOLIST_DTYPE_FROM_HEADER.items()})
		return df

	def _middle(a,b):
		return int(np.ceil((a+b)/2))

	def _import_match(self,index):
		match=self._get_match_by_index(index,newline=False)
		if match is None:
			return
		# Get season.
		season=int(match["info"]["gameVersion"].split(".")[0])

		season_filename=self._file_name_from_season(season)

		season_info_path=Path(season_filename+"."+LolDataFetcher.EXTENSION_INFOLIST)
		season_data_path=Path(season_filename+"."+LolDataFetcher.EXTENSION_DATABASE)

		# return
		# Genererate files for season if not yet existing.
		if not season_info_path.exists():
			df=self._new_season_info_list()
			serialize(df,season_info_path)
		season_data_path_str=str(season_data_path)
		if not season_data_path.exists():
			with open(season_data_path_str,"w") as file:
				pass


		start_index=os.stat(season_data_path_str).st_size
		# Append to database.
		with open(season_data_path_str,"ab") as file:
			pickle.dump(match,file,protocol=-1)
		next_start_index=os.stat(season_data_path_str).st_size
		byte_count=next_start_index-start_index

		size=next_start_index
		# pref=0
		# while size>1000 and pref<len(LolDataFetcher.PREFIXES)-1:
		# 	size=size/1000
		# 	pref+=1

		print(f" Wrote to db. New size: {size:,} B")

		# return
		# - Save byte positions
		# Append to info list.
		player=self._get_player(match)
		content={
			"match_id":match["metadata"]["matchId"],
			"created":pd.to_datetime(match["info"]["gameCreation"],unit="ms"),
			"won":self._won_this_match(match),
			"champion":player["championName"],
			"kills":player["kills"],
			"deaths":player["deaths"],
			"assists":player["assists"],
			"byte_start":start_index,
			"byte_count":byte_count,
		}
		df=deserialize(season_info_path)
		df=self._append_to_df(df,content)
		serialize(df,season_info_path)

	def _find_oldest_unsaved_match_index(self):
		index_min=0
		index_max=999
		index=index_max

		not_found=True
		while not index_min==index_max:
			# print(index_min,index,index_max)
			match=self._get_match_by_index(index)

			if match is None:
				index_max=index-1
			else:
				created=pd.to_datetime(match["info"]["gameCreation"],unit="ms")
				season=int(match["info"]["gameVersion"].split(".")[0])

				season_info_path=Path(self._file_name_from_season(season)+"."+LolDataFetcher.EXTENSION_INFOLIST)

				if not season_info_path.exists():
					index_min=index
				else:

					df=deserialize(season_info_path)

					# Look at date of last entry.
					last_entry_created=df.iloc[-1,df.columns.get_loc("created")]
					# If bigger or equal than current (meaning more recent)
					# look at smaller indices
					# else look at bigger indices.
					if last_entry_created>=created:
						index_max=index-1
					else:
						index_min=index

			index=LolDataFetcher._middle(index_min,index_max)

		return index


	def update(self):
		index=self._find_oldest_unsaved_match_index()
		print(f"Starting import with index {index}.")
		while index>=0:
			self._import_match(index)
			index-=1
